#!/bin/bash

function usage(){
    echo -e "Usage: $0 virtualenv_name\nEg. $0 test_venv"
}

function help(){
    echo -e "$0 action\n"
    echo -e "Actions:"
    echo -e "\tlist                                     - List all installed virtual envs"
    echo -e "\tadd virtual_env_name [python_version]    - Adds a virtual env to $venv_home; python_env ex: python3"
    echo -e "\tdelete virtual_env_name                  - Deletes the given virtual env"
}

function find_dependencies(){
    virtualenv_location=`echo $(which virtualenv)`
    if [[ "$virtualenv_location" == "" ]]; then
        echo -e "$0 depends on virtualenv to work.\nFor more information about virtualenv look into https://virtualenv.pypa.io/en/latest/#"
        exit 1
    fi
}

function create_venv(){
    venv_name="$1"
    python_version="$2"

    if [[ "$venv_name" == "" ]]; then
        echo "Error: virtualenv name not defined."
        usage
        exit 1;
    fi

    if [[ -d "$venv_name$suffix" ]]; then
        echo -ne "Virtual env $venv_name already exists.\nDo you want to overwrite. [y|N]\n[Warning] All installed pachkages will be lost. "
        read flag

        if [[ "y" != "$flag" ]]; then
            exit 0
        fi
        echo -e "Overwritting venv $venv"
    fi

    if [[ "$python_version" != "" ]]; then
        python_version="-p $python_version"
    fi


    virtualenv $python_version $venv_home/$venv_name$suffix
    if [[ "y" != "$flag" ]]; then
        echo "alias $venv_name='deactivate 2&>1 /dev/null; source ${venv_home}/$venv_name$suffix/bin/activate'" >> $bash_file
    fi
}

function list_venvs(){
    for venv in `ls $venv_home`; do
        echo "$venv" | sed "s/$suffix//"
    done;
}

function delete_venv(){
    venv_name=$1$suffix
    if [[ -d "$venv_home/$venv_name" ]]; then
        rm -rf "$venv_home/$venv_name"
    fi;
    cp ~/.bashrc ~/.bashrc.old
    sed "/$venv_name/d" ${HOME}/.bashrc.old > ${HOME}/.bashrc

}
