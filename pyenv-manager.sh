#!/bin/bash

venv_home=${HOME}/.venvs
action=$1
python_version=$3
venv_name=$2
bash_file=${HOME}/.bashrc
suffix="-env"

abs_path=$(readlink $0 | xargs dirname )

#importing helper file
. $abs_path/utils.sh

find_dependencies

#create home dir if now exists
if [[ ! -d "$venv_home" ]]; then
    mkdir $venv_home
fi

case $action in
    ''|-h|--help) help;;
    add) create_venv $venv_name $python_version;;
    delete) delete_venv $venv_name;;
    list) list_venvs;;
esac

. $bash_file
