#!/usr/local/bin/bats

#load helper
#function setup(){
#
#}
#
#function teardown()
#{
#
#}

@test 'add command with no parameter should return status = 1' {
    run pyenvmanager add 
    echo $status
    echo $lines
    [ "$status" -eq 1 ]
}

@test 'add command with no parameter should return an error message and usage' {
    OLD_PATH=$PATH
    PATH="/tmp"
    run pyenvmanager add
    [[ "${lines[0]}" =~ "Error: vrtualenv name not defined" ]]
    [[ "${lines[1]}" =~ "Usage: /usr/local/bin/pyenv-manager.sh virtualenv_name" ]]
    PATH=$OLD_PATH
}

@test 'add command must create a folder inside user_home venvs folder' {
    run pyenvmanager add test1
    [ -d ${HOME}/venvs/test1-env ]
}

@test 'add command must ask permission to replace folder if already exists' {
    run pyenvmanager add test1
    [ -d ${HOME}/venvs/test1-env ]
}
