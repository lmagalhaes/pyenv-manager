#!/bin/bash
BASEDIR=`dirname $0`
echo $BASEDIR
function stub()
{
    export PATH="/tmp/stubs:$PATH"
    mkdir -p /tmp/stubs
    echo -e "#!/bin/bash" > /tmp/stubs/$1
    chmod +x /tmp/stubs/$1
}

function stub_will_do(){
    stub=$1
    will_do=$2

    echo "$will_do" >> /tmp/stubs/$1
}

function destroy_stubs()
{
    rm -rf /tmp/stubs   
}

